import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Response } from '@angular/http';
import 'rxjs/Rx';

const URL = 'http://localhost:8080/api/metadata';

@Injectable()
export class ListService {

  constructor(private http:Http) {
  }

  getMetadata() {
    return this.http.get(URL)
      .map((response:Response) => response.json());
  }
}
