import { FrontendPage } from './app.po';
import { RouterTestingModule } from '@angular/router/testing';

describe('frontend App', function() {
  let page: FrontendPage;

  beforeEach(() => {
    page = new FrontendPage();
  });

  it('should display correct content', () => {
    page.navigateTo();
    expect(page.getNavbarBrand()).toEqual('File Uploader');
    expect(page.getHomeButton()).toEqual('Home');
    expect(page.getListButton()).toEqual('List');
    expect(page.getDropZoneArea()).toEqual('Drop Files Here');
  });
});
